package com.quotegenerator.healthinsurance.impl;

import com.quotegenerator.healthinsurance.impl.rules.Configs;
import com.quotegenerator.healthinsurance.impl.rules.Rules;
import com.quotegenerator.healthinsurance.pojo.Person;

public class QuoteGenerator {
	public static void generateQuote(Person person){
		
		Configs confg = new Rules().getRules(person);
		System.out.print("\n\n\nHealth Insurance Premium for "+
				(person.getGender().isFemale() ?"Ms/Mrs":
					(person.getGender().isMale() ? "Mr." : "Mr/Ms") )+person.getName()+" is "); 
		System.out.println(Math.ceil(confg.getPremium()));
	}
	
}
