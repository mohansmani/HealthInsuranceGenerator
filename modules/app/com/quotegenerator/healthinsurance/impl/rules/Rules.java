package com.quotegenerator.healthinsurance.impl.rules;

import com.quotegenerator.healthinsurance.pojo.Gender;
import com.quotegenerator.healthinsurance.pojo.Habbits;
import com.quotegenerator.healthinsurance.pojo.HealthCondition;
import com.quotegenerator.healthinsurance.pojo.Person;

public class Rules {
	
	public Configs getRules(Person person){
		Configs config = new Configs();
		double premium = 0;
		config.setBasePremium(5000);
		
		config.setIncTenure(5);
		
		if(person.getAge() < 18){
			premium = config.getBasePremium();
		}else if(person.getAge() < 25){
			premium = config.getBasePremium()+(config.getBasePremium()*0.1);
		}else if(person.getAge() < 30){ 
			premium = config.getBasePremium()+(config.getBasePremium()*0.1);
			premium = premium+(premium*0.1);
		}else if(person.getAge() < 35){
			premium = config.getBasePremium()+(config.getBasePremium()*0.1);
			premium = premium+(premium*0.1);
			premium = premium+(premium*0.1);
		}else if(person.getAge() < 40){
			premium = config.getBasePremium()+(config.getBasePremium()*0.1);
			premium = premium+(premium*0.1);
			premium = premium+(premium*0.1);
			premium = premium+(premium*0.1);
		}else {
			premium = config.getBasePremium()+(config.getBasePremium()*0.1);
			premium = premium+(premium*0.1);
			premium = premium+(premium*0.1);
			premium = premium+(premium*0.1);
			premium = premium+(premium*0.2);
		}
		
		Gender gender = person.getGender();
		if(gender.isMale()){
			premium = premium+(premium * 0.02);
//			premium = premium+(config.getBasePremium() * 0.02);
		}
		
		HealthCondition condition = person.getCondition();
		if(condition.isBloodPressure()){
			premium = premium+( premium * 0.01);
		}
		if(condition.isBloodSugar()){
			premium = premium+( premium * 0.01);
		}
		if(condition.isHypertension()){
			premium = premium+( premium * 0.01);
		}
		if(condition.isOverweight()){
			premium = premium+( premium * 0.01);
//			premium = premium+( config.getBasePremium() * 0.01);
		}
		
		
		Habbits habbits = person.getHabbits();
		if(habbits.isDailyExercise()){
			premium = premium-(premium * 0.03);
		}
		if(habbits.isAlcohol()){
			premium = premium+(premium * 0.03);
		}
		if(habbits.isDrugs()){
			premium = premium+(premium * 0.03);
		}
		if(habbits.isSmoking()){
			premium = premium+(premium * 0.03);
		}

//		System.out.println("hab "+config.getBasePremium()+" = "+premium);
		config.setPremium(premium);
		return config;
	}
}
