package com.quotegenerator.healthinsurance.impl.rules;

import com.quotegenerator.healthinsurance.pojo.Gender;

public class Configs {
	
	private int basePremium;
	private int incTenure; // for every 5 years
	private double premium;
	private Gender gender; 
	private String existingCondition;
	private boolean habbit;
	
	
	public int getBasePremium() {
		return basePremium;
	}
	public void setBasePremium(int basePremium) {
		this.basePremium = basePremium;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getExistingCondition() {
		return existingCondition;
	}
	public void setExistingCondition(String existingCondition) {
		this.existingCondition = existingCondition;
	}
	public boolean isHabbit() {
		return habbit;
	}
	public void setHabbit(boolean habbit) {
		this.habbit = habbit;
	}
	public int getIncTenure() {
		return incTenure;
	}
	public void setIncTenure(int incTenure) {
		this.incTenure = incTenure;
	}

}
