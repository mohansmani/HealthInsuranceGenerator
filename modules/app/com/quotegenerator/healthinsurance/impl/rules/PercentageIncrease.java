package com.quotegenerator.healthinsurance.impl.rules;

public class PercentageIncrease {

	private int baseAge; // less than 18 years
	private int slab1; // 18-25
	private int slab2; //25-30
	private int slab3; //30-35
	private int slab4; //35-40
	private int slab5; // 40+

	
	
	public int getBaseAge() {
		return baseAge;
	}
	public void setBaseAge(int baseAge) {
		this.baseAge = baseAge;
	}
	public int getSlab1() {
		return slab1;
	}
	public void setSlab1(int slab1) {
		this.slab1 = slab1;
	}
	public int getSlab2() {
		return slab2;
	}
	public void setSlab2(int slab2) {
		this.slab2 = slab2;
	}
	public int getSlab3() {
		return slab3;
	}
	public void setSlab3(int slab3) {
		this.slab3 = slab3;
	}
	public int getSlab4() {
		return slab4;
	}
	public void setSlab4(int slab4) {
		this.slab4 = slab4;
	}
	public int getSlab5() {
		return slab5;
	}
	public void setSlab5(int slab5) {
		this.slab5 = slab5;
	}
	
}
