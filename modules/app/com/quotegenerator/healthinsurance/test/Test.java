package com.quotegenerator.healthinsurance.test;

import java.io.File;

import com.quotegenerator.healthinsurance.impl.QuoteGenerator;
import com.quotegenerator.healthinsurance.pojo.Gender;
import com.quotegenerator.healthinsurance.pojo.Habbits;
import com.quotegenerator.healthinsurance.pojo.HealthCondition;
import com.quotegenerator.healthinsurance.pojo.Person;

public class Test {

	public static void main(String[] args) {
		
		HealthCondition condition = new HealthCondition();
		condition.setBloodPressure(false);
		condition.setBloodSugar(false);
		condition.setHypertension(false);
		condition.setOverweight(true);
		
		Habbits habbits = new Habbits();
		habbits.setAlcohol(true);
		habbits.setDailyExercise(true);
		habbits.setDrugs(false);
		habbits.setSmoking(false);
		
		Gender gender = new Gender();
		gender.setFemale(false);
		gender.setMale(true);
		gender.setOthers(false);
		
		Person person = new Person();
		person.setAge(34);
		person.setId(1);
		person.setName("Norman Gomes");
		person.setGender(gender);
		person.setCondition(condition);
		person.setHabbits(habbits);

		System.out.println("Generating Health Insurance Quote for "+person.getName());
		
		QuoteGenerator.generateQuote(person);
		
		File f = new File("t.txt");
		System.out.println(f.getAbsolutePath());
	}

}
