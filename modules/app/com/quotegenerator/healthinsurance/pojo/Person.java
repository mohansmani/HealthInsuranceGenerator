package com.quotegenerator.healthinsurance.pojo;

public class Person {
	private int id;
	private String name;
	private int age;
	private Gender gender;
	private Habbits habbits;
	private HealthCondition condition;

	public Person(){
		
	}
	
	public Person(int id, String name, int age, Gender gender, Habbits habbits,
			HealthCondition condition) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.habbits = habbits;
		this.condition = condition;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Habbits getHabbits() {
		return habbits;
	}
	public void setHabbits(Habbits habbits) {
		this.habbits = habbits;
	}
	public HealthCondition getCondition() {
		return condition;
	}
	public void setCondition(HealthCondition condition) {
		this.condition = condition;
	}
}
