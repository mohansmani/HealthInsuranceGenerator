package com.quotegenerator.healthinsurance.pojo;

public class Gender {
	private boolean male;
	private boolean female;
	private boolean others;
	
	public Gender(){
		
	}
	
	public boolean isMale() {
		return male;
	}
	public void setMale(boolean male) {
		this.male = male;
	}
	public boolean isFemale() {
		return female;
	}
	public void setFemale(boolean female) {
		this.female = female;
	}
	public boolean isOthers() {
		return others;
	}
	public void setOthers(boolean others) {
		this.others = others;
	}
}
